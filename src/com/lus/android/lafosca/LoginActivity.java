package com.lus.android.lafosca;


import org.apache.http.HttpStatus;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.lus.android.lafosca.fragments.LoginFragment;
import com.lus.android.lafosca.fragments.LoginFragment.OnLoginFormListener;
import com.lus.android.lafosca.service.LafoscaService;

public class LoginActivity extends FragmentActivity
	implements OnLoginFormListener {

	
	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		
		LoginFragment fragment = LoginFragment.newInstance();
		
		FragmentTransaction fmt = getSupportFragmentManager().beginTransaction();
		fmt.replace(android.R.id.content, fragment, "LOGIN_FRAGMENT");
		fmt.commit();
	}


	@Override
	public void onLoginRequested(String username, String password) {
		Intent service = new Intent(this, LafoscaService.class);
		service.setAction(LafoscaService.ACTION_SIGN_UP);
		service.putExtra(LafoscaService.EXTRA_RESULT_RECEIVER, new LoginServiceResultReceiver());
		service.putExtra(LafoscaService.EXTRA_USERNAME, username);
		service.putExtra(LafoscaService.EXTRA_PASSWORD, password);

		if (startService(service) != null) {
			LoginFragment fragment = 
				(LoginFragment)getSupportFragmentManager()
					.findFragmentByTag("LOGIN_FRAGMENT");
			if (fragment != null)
				fragment.setShowProgressBar(getString(R.string.login_progress), true);
		}
	}

	@Override
	public void onSignInRequested(String username, String password) {
		Intent service = new Intent(this, LafoscaService.class);
		service.setAction(LafoscaService.ACTION_SIGN_IN);
		service.putExtra(LafoscaService.EXTRA_RESULT_RECEIVER, new LoginServiceResultReceiver());
		service.putExtra(LafoscaService.EXTRA_USERNAME, username);
		service.putExtra(LafoscaService.EXTRA_PASSWORD, password);
		if (startService(service) != null) {
			LoginFragment fragment = 
				(LoginFragment)getSupportFragmentManager()
					.findFragmentByTag("LOGIN_FRAGMENT");
			if (fragment != null)
				fragment.setShowProgressBar(getString(R.string.signin_progress), true);
		}
	}
	
	private class LoginServiceResultReceiver extends ResultReceiver {

		public LoginServiceResultReceiver() {
			super(new Handler());
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			LoginFragment fragment = 
				(LoginFragment)getSupportFragmentManager()
					.findFragmentByTag("LOGIN_FRAGMENT");
		
			switch (resultCode) {
				case HttpStatus.SC_OK: {
					startActivity(new Intent(LoginActivity.this, MainActivity.class));
					finish();
					break;
				}
				
				case HttpStatus.SC_UNAUTHORIZED: {
					Toast.makeText(LoginActivity.this, 
						getString(R.string.login_unauthorized), 
						Toast.LENGTH_SHORT).show();
					break;
				}
				
				case HttpStatus.SC_CREATED: {
					startActivity(new Intent(LoginActivity.this, MainActivity.class));
					finish();
					break;
				}
				
				default: {
					
					break;
				}
			}
			
			if (fragment != null)
				fragment.setShowProgressBar(getString(R.string.login_register), false);
		}
	};
}
