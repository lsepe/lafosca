package com.lus.android.lafosca.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;

import com.lus.android.http.RESTClient;
import com.lus.android.lafosca.BuildConfig;
import com.lus.android.lafosca.pojo.Beach;
import com.lus.android.lafosca.pojo.Kid;
import com.lus.android.log.Logger;
import com.lus.android.log.LoggerManager;

public class LafoscaService extends IntentService {

	static final
	protected	String	API_URL = "http://lafosca-beach.herokuapp.com/api/v1";
	
	
	static final
	private Logger	LOG = LoggerManager.getLogger(LafoscaService.class);
	
	static final
	public String	EXTRA_RESULT_RECEIVER 		= "com.lus.android.lafosca.service.EXTRA_RESULT_RECEIVER";
	
	
	static final
	public String 	ACTION_SIGN_UP 				= "com.lus.android.lafosca.service.ACTION_SIGN_UP",
					ACTION_SIGN_IN				= "com.lus.android.lafosca.service.ACTION_SIGN_IN",
					ACTION_BEACH_OPEN			= "com.lus.android.lafosca.service.ACTION_BEACH_OPEN",
					ACTION_BEACH_CLOSE			= "com.lus.android.lafosca.service.ACTION_BEACH_CLOSE",
					ACTION_BEACH_STATUS			= "com.lus.android.lafosca.service.ACTION_BEACH_STATUS",
					ACTION_BEACH_CLEAN			= "com.lus.android.lafosca.service.ACTION_BEACH_CLEAN",
					ACTION_BEACH_CHANGE_FLAG	= "com.lus.android.lafosca.service.ACTION_BEACH_CHANGE_FLAG",
					ACTION_BEACH_THROW_NIVEA	= "com.lus.android.lafosca.service.ACTION_BEACH_THROW_NIVEA";

	static final
	public String	EXTRA_USERNAME				= "com.lus.android.lafosca.service.EXTRA_USERNAME",
					EXTRA_PASSWORD				= "com.lus.android.lafosca.service.EXTRA_PASSWORD",
					EXTRA_AUTH_TOKEN			= "com.lus.android.lafosca.service.EXTRA_AUTH_TOKEN";
	
	
	protected ResultReceiver	mReceiver	= null;
	
	
	public LafoscaService() {
		super("LafoscaService");
	}

	
	@Override
	protected void onHandleIntent(Intent intent) {
		
		Bundle extras = intent.getExtras();
        if (extras == null) {
            LOG.e("missed extras with the Intent");
            notifyResult(HttpStatus.SC_BAD_REQUEST, Bundle.EMPTY);
            stopSelf();
            return;
        }
        
        mReceiver = extras.getParcelable(EXTRA_RESULT_RECEIVER);
        if (mReceiver == null) {
            LOG.w("missed EXTRA_RESULT_RECEIVER");
            //stopSelf();
            //return;
        }
        
        String action = intent.getAction();
		if (TextUtils.isEmpty(action)) {
			LOG.e("missed extras with the Intent");
			notifyResult(HttpStatus.SC_BAD_REQUEST, Bundle.EMPTY);
            stopSelf();
            return;
		}
		
		if (action.equals(ACTION_SIGN_UP)) {
			onHandleSignUpIntent(extras);
		}
		
		else if (action.equals(ACTION_SIGN_IN)) {
			onHandleSignInIntent(extras);
		}

		else if (action.equals(ACTION_BEACH_STATUS)) {
			onHandleBeachStatusIntent(extras);
		}
		
		else if (action.equals(ACTION_BEACH_OPEN)) {
			
		}

		else if (action.equals(ACTION_BEACH_CLOSE)) {
			
		}
		
		
		else if (action.equals(ACTION_BEACH_CLEAN)) {
			
		}
		
		else if (action.equals(ACTION_BEACH_CHANGE_FLAG)) {
			
		}
		
		else if (action.equals(ACTION_BEACH_THROW_NIVEA)) {
			
		}
	}

	
	private void onHandleBeachStatusIntent(Bundle extras) {
		String token = extras.getString(EXTRA_AUTH_TOKEN);
		if (TextUtils.isEmpty(token)) {
			LOG.w("missed EXTRA_USERNAME with the Intent");
			
			SharedPreferences prefs = getSharedPreferences("LAFOSCA", 0);
			token = prefs.getString("token", "");
			
			if (TextUtils.isEmpty(token)) {
				notifyResult(HttpStatus.SC_UNAUTHORIZED, Bundle.EMPTY);
				stopSelf();
				return;
			}
		}
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", String.format("Token token=\"%s\"", token));
		
		RESTClient.Response response = 
			getBackendResponse(API_URL + "/state", RESTClient.Method.GET, Bundle.EMPTY, null, headers);
		if (response == null) {
			notifyResult(HttpStatus.SC_NO_CONTENT, Bundle.EMPTY);
			stopSelf();
			return;
		}
		
		
		Bundle data = new Bundle();
		
		int resultCode = response.getStatusCode();
		if ( resultCode == HttpStatus.SC_CREATED) {
			try {
				JSONObject jo = new JSONObject(response.getContentBody());
				
				Beach beach = new Beach();
				beach.setId(jo.getInt("id"));
				beach.setFlag(jo.getInt("flag"));
				beach.setState(jo.getInt("state"));
				beach.setDirtiness(jo.getInt("dirtiness"));
				beach.setHappiness(jo.getInt("happiness"));
				
				data.putParcelable("beach", beach);
				
				if (jo.has("kids") && !jo.isNull("kids")) {
					ArrayList<Kid> 	kids = new ArrayList<Kid>(10);
					
					JSONArray ja = jo.getJSONArray("kids");
					for (int i = 0; i < ja.length(); i++) {
						Kid kid = new Kid();
						kid.setAge(ja.getJSONObject(i).getInt("age"));
						kid.setName(ja.getJSONObject(i).getString("name"));
						
						kids.add(kid);
					}
					
					if (kids.size() > 0)
						data.putParcelableArrayList("kids", kids);
				}
			} catch (JSONException err) {
				LOG.e(err.getMessage(), err);
			}
		}
		
		notifyResult(resultCode, data);
	}
	
	
	private void onHandleSignInIntent(Bundle extras) {
		String username = extras.getString(EXTRA_USERNAME);
		if (TextUtils.isEmpty(username)) {
			LOG.w("missed EXTRA_USERNAME with the Intent");
			notifyResult(HttpStatus.SC_UNAUTHORIZED, Bundle.EMPTY);
            stopSelf();
            return;
		}
		
		String password = extras.getString(EXTRA_PASSWORD);
		if (TextUtils.isEmpty(password)) {
			LOG.w("missed EXTRA_PASSWORD with the Intent");
			notifyResult(HttpStatus.SC_UNAUTHORIZED, Bundle.EMPTY);
            stopSelf();
            return;
		}
		
		SharedPreferences prefs = getSharedPreferences("LAFOSCA", 0);
		Editor editor = prefs.edit();
		editor.clear();
		editor.commit();
		
		String entity = null;
		try {
			JSONObject jo = new JSONObject();
			jo.put("username", username);
			jo.put("password", password);
			JSONObject json = new JSONObject();
			json.put("user", jo);
			
			entity = json.toString();
			
		} catch (JSONException err) {
			LOG.e(err.getMessage(), err);
		}

		if (TextUtils.isEmpty(entity)) {
			notifyResult(-1, Bundle.EMPTY);
            stopSelf();
            return;
		}
		
		RESTClient.Response response = 
			getBackendResponse(API_URL + "/users", RESTClient.Method.POST, Bundle.EMPTY, entity, null);
		if (response == null) {
			notifyResult(HttpStatus.SC_NO_CONTENT, Bundle.EMPTY);
			stopSelf();
			return;
		}
		
		int resultCode = response.getStatusCode();
		if ( resultCode == HttpStatus.SC_CREATED) {
			editor.putString("username", username);
			editor.putString("password", password);
			editor.commit();
		}
		
		notifyResult(resultCode, Bundle.EMPTY);
	}
	
	private void onHandleSignUpIntent(Bundle extras) {
		String username = extras.getString(EXTRA_USERNAME);
		if (TextUtils.isEmpty(username)) {
			LOG.w("missed EXTRA_USERNAME with the Intent");
			notifyResult(HttpStatus.SC_UNAUTHORIZED, Bundle.EMPTY);
            stopSelf();
            return;
		}
		
		String password = extras.getString(EXTRA_PASSWORD);
		if (TextUtils.isEmpty(password)) {
			LOG.w("missed EXTRA_PASSWORD with the Intent");
			notifyResult(HttpStatus.SC_UNAUTHORIZED, Bundle.EMPTY);
            stopSelf();
            return;
		}
		
		String entity = null;
		try {
			JSONObject jo = new JSONObject();
			jo.put("username", username);
			jo.put("password", password);
			JSONObject json = new JSONObject();
			json.put("user", jo);
			
			entity = json.toString();
			
		} catch (JSONException err) {
			LOG.e(err.getMessage(), err);
		}
		
		if (TextUtils.isEmpty(entity)) {
			notifyResult(-1, Bundle.EMPTY);
            stopSelf();
            return;
		}
		
		SharedPreferences prefs = getSharedPreferences("LAFOSCA", 0);
		Editor editor = prefs.edit();
		editor.remove("token");
		editor.commit();
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Basic dXNlcjpwYXNzd29yZA==");
		
		RESTClient.Response response = 
			getBackendResponse(API_URL + "/user", RESTClient.Method.GET, Bundle.EMPTY, entity, headers);
		if (response == null) {
			notifyResult(HttpStatus.SC_NO_CONTENT, Bundle.EMPTY);
			stopSelf();
			return;
		}
		
		int resultCode = response.getStatusCode();
		if ( resultCode == HttpStatus.SC_OK) {
			String authenticationToken = null;
			try {
				JSONObject jo = new JSONObject(response.getContentBody());
				authenticationToken = jo.getString("authentication_token");
			} catch (JSONException err) {
				LOG.e(err.getMessage(), err);
			}
			
			if (!TextUtils.isEmpty(authenticationToken)) {
				editor.putString("username", username);
				editor.putString("token", authenticationToken);
				editor.commit();
			}
		}
		
		notifyResult(resultCode, Bundle.EMPTY);
	}
	
	
	protected void notifyResult(final int resultCode, final Bundle resultData) {
		if (mReceiver != null) {
			mReceiver.send(resultCode, resultData);
		}
	}
	
	
		
	protected RESTClient.Response getBackendResponse(
		final String url, final RESTClient.Method method, 
		final Bundle params, final String entity,
		final Map<String, String> headers) {
		
		RESTClient.Response response = null;
		RESTClient 			rest 	 = null;
		try {
			rest = new RESTClient(url);
			rest.setDebug(BuildConfig.DEBUG);
			rest.method(method);
			
			rest.header("Accept", "application/json");
			if (headers != null && headers.size() > 0) {
				for (Map.Entry<String, String> entry: headers.entrySet()) {
					rest.header(entry.getKey(), entry.getValue());
				}
			}
			
			rest.data(params);

			if (!TextUtils.isEmpty(entity)) {
				rest.header("Content-Type", "application/json");
				rest.entity(entity);
			}
			
			response = rest.execute();
		} finally {
			if (rest != null) rest.shutdown();
		}
		
		if (response.getStatusCode() == -1) {
			Bundle resultData = new Bundle();
			if (!TextUtils.isEmpty(response.getStatusLine()))
				resultData.putString(Intent.EXTRA_TEXT, response.getStatusLine());
			notifyResult(-1, resultData);
			return null;
		}
		
		return response;
	}
	
	protected boolean isWifiOr3GEnabled() {
		ConnectivityManager cm = 
			(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		
		boolean is3g = false;
		try {
			is3g = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.isConnectedOrConnecting();	
		} catch (Exception err) {
			LOG.w(err.getMessage());
		}
		
		boolean isWifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
			.isConnectedOrConnecting();
		
		return (is3g || isWifi);
	}
}
