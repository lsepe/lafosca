package com.lus.android.lafosca;


import org.apache.http.HttpStatus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.view.Window;

import com.lus.android.lafosca.service.LafoscaService;
import com.lus.android.log.Logger;
import com.lus.android.log.LoggerManager;

public class MainActivity extends FragmentActivity {

	static final
	private Logger	LOG = LoggerManager.getLogger();
	
	
	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);

		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		
		SharedPreferences prefs = getSharedPreferences("LAFOSCA", 0);
		LOG.d("user: <%s>", prefs.getString("username", ""));
		
		Intent service = new Intent(this, LafoscaService.class);
		service.setAction(LafoscaService.ACTION_BEACH_STATUS);
		service.putExtra(LafoscaService.EXTRA_RESULT_RECEIVER, new BeachStatusResultReceiver());
		service.putExtra(LafoscaService.EXTRA_AUTH_TOKEN, prefs.getString("token", ""));
		if (startService(service) != null) {
			setProgressBarIndeterminateVisibility(true);
		}
		
	}
	
	
	private class BeachStatusResultReceiver extends ResultReceiver {
		
		public BeachStatusResultReceiver() {
			super( new Handler());
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			setProgressBarIndeterminateVisibility(false);
			
			switch (resultCode) {
				case HttpStatus.SC_OK: {
					if (!resultData.isEmpty()) {
						
					}
					break;
				}
				
				case HttpStatus.SC_UNAUTHORIZED: {
					Intent intent = new Intent(MainActivity.this, LoginActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					finish();
					break;
				}
			
				default: {
					
					break;
				}
			}
		}
	};
}
