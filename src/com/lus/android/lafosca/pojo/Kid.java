package com.lus.android.lafosca.pojo;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class Kid implements Parcelable, Comparable<Kid> {
	
	private int 	mAge 		= 0;
	private String	mName		= null;
	  
	public Kid() {  }
	
	public int getAge() { return mAge; }
	public void setAge(int value) { mAge = value; }
	
	public String getName() { return (TextUtils.isEmpty(mName) ? "" : mName); }
	public void setName(String value) { mName = value; }
	
	@Override
	public int describeContents() { return 0; }
	
	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeInt(mAge);
		parcel.writeString(mName);
	}

	static
	public Creator<Kid> CREATOR = new Creator<Kid>() {

		@Override
		public Kid createFromParcel(Parcel source) {
			return new Kid(source);
		}

		@Override
		public Kid[] newArray(int size) {
			return new Kid[size];
		}
	};
	
	@Override
	public int compareTo(Kid another) {
		return (mAge - another.getAge());
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Kid) {
			return getName().equals(((Kid) o).getName());
		}
		return false;
	}

	@Override
	public int hashCode() { return getName().hashCode(); }

	@Override
	public String toString() { return getName(); }

	private Kid(Parcel in) {
		mAge	= in.readInt();
		mName	= in.readString();
    }

}
