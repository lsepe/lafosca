package com.lus.android.lafosca.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class Beach implements Parcelable {

	public enum State {
		OPEN,
		CLOSED,
	};
	
	public enum Flag {
		GREEN,
		YELLOW,
		RED,
	};
	
	private int 	mId 		= 0;
	private State	mState		= State.CLOSED;
	private Flag	mFlag		= Flag.RED;
	private int		mHappines	= 0;
	private int		mDirtiness	= 100;
	  
	public Beach() {  }
	
	public int getId() { return mId; }
	public void setId(int value) { mId = value; }
	

	public State getState() { return mState; }
	
	public void setState(int value) {
		try {
			mState = State.values()[value];
		} catch (Exception err) {
			mState = State.CLOSED;
		}
	}
	
	public void setState(State value) { mState = value; }
	

	public Flag getFlag() { return mFlag; }
	
	public void setFlag(int value) {
		try {
			mFlag = Flag.values()[value];
		} catch (Exception err) {
			mFlag = Flag.RED;
		}
	}
	
	public void setFlag(Flag value) { mFlag = value; }

	
	public int getHappiness() { return mHappines; }
	public void setHappiness(int value) { mHappines = value; }

	
	public int getDirtiness() { return mDirtiness; }
	public void setDirtiness(int value) { mDirtiness = value; }

	
	@Override
	public int describeContents() { return 0; }
	
	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeInt(mId);
		parcel.writeInt(mState.ordinal());
		parcel.writeInt(mFlag.ordinal());
		parcel.writeInt(mHappines);
		parcel.writeInt(mDirtiness);
	}

	static
	public Creator<Beach> CREATOR = new Creator<Beach>() {

		@Override
		public Beach createFromParcel(Parcel source) {
			return new Beach(source);
		}

		@Override
		public Beach[] newArray(int size) {
			return new Beach[size];
		}
	};
	
	
	private Beach(Parcel in) {
		mId	= in.readInt();
		setState(in.readInt());
		setFlag(in.readInt());
		mHappines = in.readInt();
		mDirtiness = in.readInt();
    }
}
