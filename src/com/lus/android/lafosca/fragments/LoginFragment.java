package com.lus.android.lafosca.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lus.android.lafosca.R;



public class LoginFragment extends Fragment {

	public interface OnLoginFormListener {
		void onLoginRequested(String username, String password);
		void onSignInRequested(String username, String password);
	};
	
	static final
	public LoginFragment newInstance() {
		LoginFragment f = new LoginFragment();		
		return f;
	}
	
	private TextView				mExtraInfo		= null;
	private ProgressBar				mProgressBar 	= null;
	
	private TextView				mUsername		= null;
	private TextView				mPassword		= null;
	
	private OnLoginFormListener		mCallback		= null;
	
	
	public LoginFragment() {
		
	}
	
	
	public void setShowProgressBar(String message, boolean value) {
		View rootView = getView();
		if (rootView == null) return;
		
		if (!TextUtils.isEmpty(message)) mExtraInfo.setText(Html.fromHtml(message));
		else mExtraInfo.setText(getString(R.string.please_wait));
		
		rootView.findViewById(R.id.login_data).setVisibility((value ? View.INVISIBLE : View.VISIBLE));
		mProgressBar.setVisibility((value ? View.VISIBLE : View.INVISIBLE));
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try {
			mCallback = (OnLoginFormListener)activity;
		} catch (ClassCastException ignore) {  }
	}
	

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		setShowProgressBar(getString(R.string.login_register), false);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.login_fragment, container, false);
	
		mProgressBar = (ProgressBar)rootView.findViewById(R.id.login_progress);

		mExtraInfo = (TextView)rootView.findViewById(R.id.login_extra_info);
		
		mUsername = (TextView)rootView.findViewById(R.id.login_username);
		mPassword = (TextView)rootView.findViewById(R.id.login_password);
		
		View bt = rootView.findViewById(R.id.login_button);
		bt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String username = mUsername.getText().toString().trim();
				String password = mPassword.getText().toString().trim();
				
				if (TextUtils.isEmpty(username)) return;
				if (TextUtils.isEmpty(password)) return;
				
				if (mCallback != null) {
					mCallback.onLoginRequested(username, password);
				}
			}
		});
		
		mExtraInfo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String username = mUsername.getText().toString().trim();
				String password = mPassword.getText().toString().trim();
				
				if (TextUtils.isEmpty(username)) return;
				if (TextUtils.isEmpty(password)) return;
				
				if (mCallback != null)
					mCallback.onSignInRequested(username, password);
			}
		});
		
		return rootView;
	}

}
